---
title: "Installing the development version of Python CLI applications"
date: 2021-05-20T13:16:49+02:00
slug: dev-install-python-apps

---

The task to solve is to install a development version of a Python CLI
application, so that you can try it out. This is handy when you are working on
a change for the app, or you would like to try out some unreleased version
of it.

There are multiple ways to do this, probably the simplest one is to install
the app from its Git repository, via `pip`, in the Python user install
directory, in editable mode:

    $ pip install --user --editable .

The above stops working once you would like to have a version of the same app
installed using the OS's package manager (`dnf` on Fedora Linux), or there are
multiple apps you would like to have installed and their dependencies get
tangled.

I've solved the above by installing such applications in their dedicated
virtual environment, aka [venv]. Virtualenvwrapper is a tool which
streamlines working with virtual environments:

    $ sudo dnf install python3-virtualenvwrapper

Now I'm ready to create a virtualenv. For [packit] I did:

    $ mkvirtualenv packit-dev

The command above should have already activated the virtualenv (which is the
case if your prompt displays the name of the virtualenv). If not, you should
activate it first:

    $ workon packit-dev

Now navigate to the directory where the source code of the app is cloned and
install the application with `pip`, in editable mode:

    $ pip install --editable .

As the command above is called from an active virtualenv, the executable for
`pip` is from the virtualenv, and the application is going to be installed in
the virtualenv.

    $ which pip
    ~/.virtualenvs/packit-dev/bin/pip

    $ which packit
    ~/.virtualenvs/packit-dev/bin/packit

Calling the above executable of the application will use dependencies
installed in the virtualenv, even when the environment is not active. You can
tell this by looking at the shebang of the file:

    $ head -n 1 $(which packit)
    #!/home/hcsomort/.virtualenvs/packit-dev/bin/python

So at this point all you have to do is to create a symbolic link pointing to
this file for easier access.

Use some name which makes it clear that this is a dev version of the app. I
use `packit-dev` in this example. This way I have easy access both to the
officially released `packit`, installed via `dnf` and the latest development
version.

The symbolic link should be in some place in your `PATH`. This is usually
`~/bin` or `~/.local/bin`. You might need to create the directory first.

    $ ln -s $(which packit) ~/bin/packit-dev

Now you can deactivate the virtualenv. The dev version of the app is ready to
be used:

    $ deactivate
    $ which packit-dev
    ~/bin/packit-dev

Using `--editable` above makes it possible to try out some other version of
the app just by switching branches in the Git repo. This is an easy way to try out
the PRs of my teammates.

If a PR introduces some new dependencies, though, there is a little bit more
to do: reactivate the virtualenv and re-install the app.

    $ workon packit-dev
    $ pip install --editable --upgrade .
    $ deactivate

*This blog post was written b/c of [Stef's PR].*

[venv]: https://docs.python.org/3/tutorial/venv.html
[packit]: https://github.com/packit/packit
[Stef's PR]: https://github.com/packit/packit/pull/1247
