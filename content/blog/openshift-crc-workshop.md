---
title: "OpenShift & CRC Workshop"
date: 2020-11-06T14:59:42+01:00
draft: false
---

Notes of a workshop where I showed my team ([Packit](https://packit.dev)) how
I work with *CodeReady Containers* and *OpenShift*.

# CRC

Installation: [follow the
steps](https://cloud.redhat.com/openshift/install/crc/installer-provisioned)
to download, setup and run a CodeReady Containers cluster. You need a Red Hat
account to log in and access the page. Reading through [the
docs](https://access.redhat.com/documentation/en-us/red_hat_codeready_containers/)
might save some time with fixing issues.

Unpack the executable from the archive to somewhere in your `PATH`. I have it
in `~/.local/bin`.

The *pull secret* needs to be entered during the first `crc start`.

I need to run:

```bash
$ sudo systemctl start libvirtd
```

... before starting the cluster, but couldn't figure it out why 😕

`crc` commands are similar to the ones from `minishift`. Use `crc --help` to
learn more about them.

While working on *dist2src*, the deployment kept running out of memory,
especially during re-deployments. Raising the memory available for the
CRC-cluster solved this issue:

```bash
crc start --memory 10240
```

The `Starting OpenShift cluster...` step is going to take minutes. Go get a ☕.

Once the cluster is up, just follow the instructions:

```bash
To access the cluster, first set up your environment by following 'crc oc-env' instructions.
Then you can access it by running 'oc login -u developer -p developer https://api.crc.testing:6443'.
To login as an admin, run 'oc login -u kubeadmin -p dpDFV-xamBW-kKAk3-Fi6Lg https://api.crc.testing:6443'.
To access the cluster, first set up your environment by following 'crc oc-env' instructions.

You can now run 'crc console' and use these credentials to access the OpenShift web console.
```

Personally, I didn't find the need to use the `crc oc-env` part, ever, `oc`
available on Fedora works just fine, but if I would ever hit strange issues
when interacting with the cluster, this would be the first thing to try in
order to fix them.

# OpenShift

Run `oc project` to tell which project and cluster is active. For switching
between them, I use [a context switcher
script](https://csomh.gitlab.io/blog/evolution-of-an-openshift-context-switcher/).

## Working with pods

`oc get pods` will list pods in the project and their status. The `-w` flag
can be used to enter *watch mode*, but I find running `watch -n5 oc get pods`
in a *tmux-pane* of it's own much more intuitive.

In general, I use *tmux-panes* to set up a dashboard to work with a project,
in case I plan to stay in the terminal.

`oc describe pod <POD>` shows all you need to know about a pod. But for this,
I actually prefer to use the web-console 😅.

`oc logs -f [pod/]<POD>` to stream the logs. But the pod name will change
(most of the time) when things are redeployed. So an alternative is to follow
the logs for the deployment config with `oc logs -f dc/<DEPLOYMENTCONFIG>`.

## Working with other resources

`oc get <RESOURCE>` - to list things.

`oc edit <RESOURCE>` - to edit those things.

A resource can a ConfigMap (`cm`), DeploymentConfig (`dc`), Secret (`secret`)
and so on. See `oc types` for a full list.

Don't forget that editing ConfigMaps and Secrets doesn't always redeploy the
pods that are using them, so you might need to do this manually.

```bash
$ oc rollout latest dc/<NAME>
```

## Defining objects

`oc explain` is great for getting some help and understanding OpenShift YAML.

```bash
$ oc explain sts.spec.template.spec.containers.envFrom
KIND:     StatefulSet
VERSION:  apps/v1

RESOURCE: envFrom <[]Object>

DESCRIPTION:
     List of sources to populate environment variables in the container. The
     keys defined within a source must be a C_IDENTIFIER. All invalid keys will
     be reported as an event when the container is starting. When a key exists
     in multiple sources, the value associated with the last source will take
     precedence. Values defined by an Env with a duplicate key will take
     precedence. Cannot be updated.

     EnvFromSource represents the source of a set of ConfigMaps

FIELDS:
   configMapRef <Object>
     The ConfigMap to select from

   prefix       <string>
     An optional identifier to prepend to each key in the ConfigMap. Must be a
     C_IDENTIFIER.

   secretRef    <Object>
     The Secret to select from
```
