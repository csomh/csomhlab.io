---
title: "ffmpeg-commands for editing video and audio"
date: 2020-07-06T08:26:33+02:00
draft: false
---

I was using [Pitivi] in the last few months to edit demos and family videos.
But Pitivi doesn't have it all, so sometimes it's just easier to turn to the
command line and use `ffmpeg`. Bellow are few of the commands I've used for
future reference.

Extract audio from video:

```
$ ffmpeg -i input-video.avi -vn -acodec copy output-audio.aac
```

Get the first frame as a picture (good when you need to freeze the screen for
a while):

```
$ ffmpeg -i video.mkv -vf "select=eq(n\,0)"  picture.jpg
```

Removing all audio channels from a video:

```
$ ffmpeg -i video-with-sound.mp4 -map 0 -map -0:a -c copy video-without-sound.mp4
```

Convert AAC sound to WAV:

```
$ ffmpeg -i sound.aac  sound.wav
```

Crop audio/video:

```
$ ffmpeg -i sound.opus -ss 00:00:00 -to 00:01:11 -c copy cropped.opus
```

And here is [a collection] from [protrolium] with even more commands.

[Pitivi]: http://www.pitivi.org/
[a collection]: https://gist.github.com/protrolium/e0dbd4bb0f1a396fcb55
[protrolium]: https://gist.github.com/protrolium
