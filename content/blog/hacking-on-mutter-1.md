---
date: 2022-11-10T10:30:00+01:00
title: "Hacking on Mutter #1: setting up the dev env"
slug: hacking-on-mutter-1
draft: false
---

Last week I started a embedment with one of the Automotive teams at Red Hat.
Until the end of January 2023 I'll be focusing on learning about [mutter], with
the goal to be able to fix [this DRM-leasing issue].

Having spent the last 6 years programming mostly in Python most of the tools
and technologies I'll need to work with are new to me. To get some insight in
how I'm going to deal with this situation, I'll start a series of blog posts
documenting the steps and process I'm following to make progress on this
issue. My hope is that doing so I'll be able to debug and improve my own
thinking and problem solving skills. And maybe these blogs will be useful for
others, too.

This first post is about setting up a development environment for mutter in
order to be able to compile it, run the tests and run mutter in nested mode so
that I can try it out as I make changes to the code.

## Build a dev env with the help of toolbox

I choose to do the dev env setup in a [toolbox container] in order to isolate
it from my host. I've created a new toolbox container with Fedora Linux 37 and
refreshed all its content:

    $ toolbox create --distro fedora --release 37 mutter-dev
    $ toolbox enter mutter-dev
    $ sudo dnf upgrade --refresh

Now come the `mutter` dependencies.

My first approach was to run `meson setup build`, wait for it to fail, `dnf
install` the missing dependency, and repeat, until everything turned green.
This was a quite a lengthy process, as you can imagine. Then I had a chat with
Bilal to learn about how to run the tests. Curious of other possible
approaches, I've told him about how I got to compile mutter, to which his
response was: "Or you could use `dnf builddep mutter`". _Facepalm_, yes, of
course:

    $ sudo dnf builddep mutter

The above command also installs `meson` and `gcc`, so both the build tool and
the compiler are available.

## Compile `mutter` and run `mutter --nested`

With all the dependencies in place, compile and install the compiled binaries:

    $ meson setup build
    $ meson compile -C build
    $ sudo meson install -C build

Running `mutter --nested` fails with 

    mutter: error while loading shared libraries: libmutter-11.so.0: cannot open shared object file: No such file or directory

I do a few searches to refresh my knowledge regarding shared libraries on
Linux, and I decide to set `LD_LIBRARY_PATH` to `/usr/local/lib64`. (There
might be some other way to fix this.)

`mutter --nested` works now 🎉

## Run the tests and start apps in the nested compositor

These steps I've learnt from Bilal.

This is the command to run the tests:

    $ ./src/tests/meta-dbus-runner.py xvfb-run meson test -C build --setup plain

… and requires `python3-dbusmock` to be installed:

    $ sudo dnf install python3-dbusmock

To run a single test specify the name of the test like this:

    $ ./src/tests/meta-dbus-runner.py xvfb-run meson test -C build --setup plain actor-color-state

In order to start an app in the nested compositor, set `WAYLAND_DISPLAY` to
the name of the display the nested compositor is using. This is usually
`wayland-1`, but it can be checked in the start-up logs of mutter.

    $ WAYLAND_DISPLAY=1 gnome-calculator

[mutter]: https://gitlab.gnome.org/GNOME/mutter
[this DRM-leasing issue]: https://gitlab.gnome.org/GNOME/mutter/-/issues/1743
[toolbox container]: https://containertoolbx.org/install/
