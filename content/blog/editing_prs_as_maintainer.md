---
title: "Editing PRs as a maintainer"
date: 2020-03-06T09:59:52+01:00
---

GitHub has this [really neat feature], where contributors to a project can
allow maintainers of the project to push to those branches in their forks for
which a PR was submitted.

Unfortunately the UI doesn't really give any hint for maintainers whether they
can do this, and the [instructions from GitHub] on how to do this, are a little
bit bloated, in the sense that it requires you to clone the fork.

But there is an easier way—not requiring a whole clone of the repo and 11
steps to complete—to do this within your own local copy of the code, using
`git` commands. The only challenge being, that you have to use git in a
slightly different way than usual.

So here it is, how it will work:

0. First: make sure your local copy of the repository is up to date with the
   upstream.

1. Fetch the branch you want to edit to a local branch:

```
    $ git fetch git@github.com:shreyaspapi/packit.git \
        +issue701:shreyaspapi-packit-issue701
```

2. Checkout and modify the local branch. In this case rebase it to `master`:

```
    $ git checkout shreyaspapi-packit-issue701
    $ git rebase master
```

3. Push the change back to the original branch. This is where you'll find out
   if you actually have the permissions to do it :)

```
    $ git push --force-with-lease git@github.com:shreyaspapi/packit.git \
        +shreyaspapi-packit-issue701:issue701
```

While reading through the man pages in order to figure out how to do the
above, I've learnt [how `<refspec>` works]. That's the thing at the end of the
`fetch` and `push` commands above, and this is its syntax:

```
    +<src>:<dest>
```

In the case of `fetch`, I've used `<refspec>` to specify which remote branch
to fetch to which local branch. In the case of `push`, the other way around:
which local branch to push to which remote branch.

I do realize that there are many more [ways] for doing this—that's the
beauty of `git`, I guess :) I will probably stick to using the commands above,
because I find them explicit (less likely to make a mistake) and easy to
remember.

[instructions from GitHub]: https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/committing-changes-to-a-pull-request-branch-created-from-a-fork
[how `<refspec>` works]: https://git-scm.com/docs/git-fetch#Documentation/git-fetch.txt-ltrefspecgt
[really neat feature]: https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/allowing-changes-to-a-pull-request-branch-created-from-a-fork
[ways]: https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html#pushing-to-the-fork-as-the-upstream-member
